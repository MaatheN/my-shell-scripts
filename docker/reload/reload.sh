#!/bin/bash

####################
#functions

#extract the designation's name from this output type : 
# dname_directory:[..]
function getDesignationName {
	echo ${1%%[_]*}
}
#return the length of the prefix of a line. 1 argument : line content (string)
function getPrefixLength {
	prefixLength=${1%%[:]*}
	prefixLength=${#prefixLength}
	echo $prefixLength
}
#return imageName from designations.txt with 1 argument : designationName (string)
function getImageName {
	imageNameLine=`cat $HOME/sh/docker/reload/designations.txt | grep $1_imageName`
	imageName=${imageNameLine:$(getPrefixLength $imageNameLine)+2}
	echo $imageName
}
#return url from designations.txt with 1 argument : designationName (string)
function getUrl {
	urlLine=`cat $HOME/sh/docker/reload/designations.txt | grep $1_url`
	url=${urlLine:$(getPrefixLength $urlLine)+2}
	echo $url
}
function showHelp {
	echo "RELOAD.SH HELP"
	echo ""
	echo "SYNOPSIS"
	echo "	reload.sh [-q]"
	echo ""
	echo "DESCRIPTION"
	echo "	Running this script in a docker app directory launch the Makefile build after deleting every concerned old containers."
	echo "	The goal is to refresh your web app page in one line from terminal."
	echo ""
	echo "DESIGNATION FILE"
	echo "	First, you have to give few app infos(*) via the designation.txt file located in the script directory" 
	echo "	($HOME/sh/docker/reload/designations.txt)."
	echo "	If the file doesnt exist yet, run the script to get it with a data model inside."
	echo "	(* an app name (unique), url (page to reload), directory (app location) & image name (of the docker app container))"
	echo ""
	echo "OPTION"
	echo "	-q : stop & remove related containers without relaunching the app."
	echo ""
}
####################

clear
if [ "$1" = "--help" ]; then
	showHelp
	exit 0
fi
#1.1 check if designation file exist and is not empty else create an example
reloadDirectoryList=`ls $HOME/sh/docker/reload | grep designations.txt`
if [ ${#reloadDirectoryList} -lt 2 ]; then
	echo "designations.txt does not exist in $HOME/sh/docker/reload"
	echo "creating a new one.."
	echo "uniqueAppName{
		uniqueAppName_name:uniqueAppName
		uniqueAppName_url:http://localhost:port/...#page rendering app url
		uniqueAppName_directory:#absolute app path 
		uniqueAppName_imageName:#docker image name of the container
	}" > $HOME/sh/docker/reload/designations.txt
	echo "..done"
	echo "please fill its data model before relaunching this script or reload.sh --help"
	echo ""
	exit 1
fi
#1.2 find designation's data with pwd
cd=`pwd`
directoryLine=`cat $HOME/sh/docker/reload/designations.txt | grep $cd`
	#1.b check if cd exist in designation.txt
if [ ${#directoryLine} -eq 0 ]; then
	echo "error : there is no designate app in this directory."
	echo "Please add your app in the designation.txt file"
	echo "reload.sh --help for more infos"
	exit 1
else
	#2. stop and remove containers from designation's imageName 
	designationName=${directoryLine%%[_]*}
	echo "stopping and removing $(getImageName $designationName) related container(s) & image(s)"
	containersId=`docker ps -aq --filter ancestor=$(getImageName $designationName)`
	if [ ${#containersId} -lt 10 ]; then
		echo "No containers to stop"
	else
		docker rm --force $(docker ps -aq --filter ancestor=$(getImageName $designationName))
		echo "CONTAINER(S) STOPED"
		docker rmi -f $(getImageName $designationName)
		echo "IMAGE(S) STOPED"
	fi
	if [ "$1" = "-q" ]; then
		echo "exiting script before refreshing app.."
		exit 0
	else
		#3. build and run app
		#4. refresh the page
		getUrl $designationName
		url=$(getUrl $designationName)
		echo "url : $url"
		make build
		make run &
		sleep 2.5 ; open -a Safari $url
		#make build 
		#open -a Safari $url
		#make run && echo "RUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUNRUUUUUUUUUUUN"
		#make run && sleep 4 ; open $url
		#make run && open http://localhost:5000/en/Decision_Making
		#(sleep 5 ; open http://localhost:5000/en/Decision_Making) && make run
	fi
fi